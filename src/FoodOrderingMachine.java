import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton sushiButton;
    private JButton ramenButton;
    private JButton sukiyakiButton;
    private JButton yakitoriButton;
    private JButton sobaButton;
    private JButton takoyakiButton;
    private JLabel orderedItems;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JLabel totalPriceLabel;
    private JLabel sushiLabel;
    private JLabel ramenLabel;
    private JLabel sukiyakiLabel;
    private JLabel yakitoriLabel;
    private JLabel sobaLabel;
    private JLabel takoyakiLabel;
    String currentText;
    int totalPrice = 0;

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            totalPrice += price;
            currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + "\t" + price + " yen" + "\n");
            totalPriceLabel.setText("Total Price:    " + totalPrice + " yen");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food +"! " + "It will be served as soon as possible.");
        }
    }

    public FoodOrderingMachine() {
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi", 700);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);
            }
        });
        sukiyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sukiyaki", 900);
            }
        });
        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitori", 600);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba", 500);
            }
        });
        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki", 400);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + totalPrice + " yen.");
                    totalPrice = 0;
                    orderedItemsList.setText("");
                    totalPriceLabel.setText("Total Price:    " + totalPrice + " yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
